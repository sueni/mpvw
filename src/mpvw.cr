require "./mpvw/*"

instance = Instance.new ARGV
ActionModel.apply instance
socket_file = instance.socket_file

at_exit { socket_file.close }
Signal::INT.trap { exit }
Signal::TERM.trap { exit }

argument_list = ArgumentList.new ARGV
argument_list.apply_profiles

Mpv.new(argument_list.strings, socket_file.path).play
