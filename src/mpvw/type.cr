enum Type
  Top
  Norm
  Low

  def same_types
    Array.new 1, self
  end

  def lower_types
    Type.values.select { |other| other.value > self.value }
  end

  def higher_types
    Type.values.select { |other| other.value < self.value }
  end

  def name
    self.to_s
  end

  def has_playing?
    socket_files.any? do |sf|
      existing_socket = ExistingSocket.new sf
      if active_socket = existing_socket.active
        SocketAPI.new(active_socket).playing?
      end
    end
  end

  private def socket_files
    Dir.glob(
      String.build do |s|
        s << ENV["TMPDIR"] << '/'
        s << SocketFile::PREFIX
        s << name << '*'
      end
    )
  end
end
