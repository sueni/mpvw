struct Argument
  def initialize(@argument : String)
    @argument = abs_path if File.exists? @argument
  end

  def option?
    @argument.starts_with? "--"
  end

  def url?
    @argument.includes? "//"
  end

  def empty?
    @argument.empty?
  end

  def to_s
    @argument
  end

  def abs_path
    File.expand_path @argument
  end

  def audio_file?
    %w[.flac .ogg .mp3].includes?(
      File.extname(@argument).downcase
    )
  end

  def temp_file?
    abs_path.starts_with? "/tmp/"
  end
end
