module ActionModel
  extend self

  def apply(instance : Instance)
    type = instance.type
    execute type.same_types, "set pause yes"
    execute type.lower_types, "quit-watch-later"
    instance.mute if instance.has_active_superiors?
  end

  private def execute(types, action)
    types.each do |type|
      Process.new(
        "mpvc",
        {"-ALL", "-#{type.name}", "--action", action}
      ).wait
    end
  end
end
