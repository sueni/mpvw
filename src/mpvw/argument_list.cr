struct ArgumentList
  @argument_list : Array(Argument)

  def initialize(argv : Array(String))
    @argument_list = argv.map { |arg| Argument.new arg }
  end

  def strings
    @argument_list.map &.to_s
  end

  private def contain_options_only?
    @argument_list.all? &.option?
  end

  def apply_profiles
    case
    when contain_audio_files?
      @argument_list << Argument.new "--profile=local-audio"
    when contain_temp_files?
      @argument_list << Argument.new "--profile=webm"
    end
  end

  private def contain_audio_files?
    @argument_list.any? &.audio_file?
  end

  private def contain_temp_files?
    @argument_list.any? &.temp_file?
  end
end
