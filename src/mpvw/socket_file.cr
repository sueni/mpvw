struct SocketFile
  PREFIX = "mpv_socket_"

  def initialize(type : Type)
    @socket_file = File.tempfile PREFIX + type.name, ""
  end

  def close
    return unless File.exists? @socket_file.path
    @socket_file.delete
  end

  delegate path, to: @socket_file
end
