require "json"
require "./existing_socket"

struct SocketAPI
  def initialize(socket : UNIXSocket)
    @socket = socket
  end

  def playing?
    !unwrap(request("pause")).as_bool
  end

  private def unwrap(response)
    JSON.parse(response)["data"]
  end

  private def request(property)
    @socket.puts wrap(property)
    @socket.gets.not_nil!
  end

  private def wrap(property)
    {command: ["get_property", property]}.to_json
  end
end
