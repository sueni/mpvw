struct Mpv
  def initialize(@args : Array(String), socket_file)
    @args << "--input-ipc-server=#{socket_file}"
    @mem = IO::Memory.new
  end

  def play
    return play_interactive if STDIN.tty?
    play_in_background
  end

  private def play_in_background
    Process.run("mpv",
      args: @args,
      output: @mem,
      chdir: ENV["MYTMPDIR"])

    notify if @args.includes? "--profile=online"
  end

  private def play_interactive
    exit Process.run("mpv",
      args: @args,
      input: STDIN,
      output: STDOUT,
      error: STDERR,
      chdir: ENV["MYTMPDIR"]).exit_code
  end

  private def notify
    if (output = mpv_output)
      Process.run(
        "notify-send",
        {"-u", "critical", "-t", "3000", output}
      )
    end
  end

  private def mpv_output
    @mem.rewind
    @mem.gets
  ensure
    @mem.close
  end
end
