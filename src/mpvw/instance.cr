struct Instance
  getter type : Type

  def initialize(argv : Array(String))
    @type = pick_type(argv)
  end

  def mute
    ARGV << "--mute=yes"
  end

  def pick_type(argv)
    case argv
    when .delete("-Top")  then Type::Top
    when .delete("-Norm") then Type::Norm
    when .delete("-Low")  then Type::Low
    else                       Type::Norm
    end
  end

  def socket_file : SocketFile
    SocketFile.new type
  end

  def has_active_superiors?
    type.higher_types.any? &.has_playing?
  end
end
