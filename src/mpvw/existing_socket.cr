require "socket"

struct ExistingSocket
  @active_socket : UNIXSocket?

  def initialize(socket_file : String)
    @socket_file = socket_file
  end

  def active
    test_connection
    @active_socket
  end

  private def test_connection
    @active_socket = UNIXSocket.new @socket_file
  rescue Errno
    File.delete @socket_file
  end
end
