CRYSTAL ?= crystal
BINDIR = ~/.bin
SRCDIR = $(CURDIR)/src
NAME = $(shell basename $(CURDIR))
TARGET = $(BINDIR)/$(NAME)
DEV_TARGET = $(MYTMPDIR)/$(NAME)

$(TARGET): $(SRCDIR)/$(NAME).cr $(SRCDIR)/$(NAME)/*.cr
	@mkdir -p $(BINDIR)
	@$(CRYSTAL) build --no-color --release --no-debug $(SRCDIR)/$(NAME).cr -o $(TARGET)

.PHONY: dev
dev:
	@$(CRYSTAL) build --no-color $(SRCDIR)/$(NAME).cr -o $(DEV_TARGET)

.PHONY: check
check:
	@$(CRYSTAL) run --no-color --no-codegen $(SRCDIR)/$(NAME).cr
